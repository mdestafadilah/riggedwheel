import {Wheel} from 'react-custom-roulette'
import {Input, Button, Card, Timeline} from 'antd'
import React, {useState} from 'react'
import Ruimte from './resources/Space'
import {Row, Col, Container} from 'react-bootstrap'
import CardCenter from "./resources/CardCenter"


function App() {
    const [people, setPeople] = useState(peopleData)
    const [amtPicked, setAmtPicked] = useState(0)
    const [amtTagged, setAmtTagged] = useState(0)
    const [amtFirst, setAmtFirst] = useState(0)
    const [mustSpin, setMustSpin] = useState(false)
    const [spinData, setSpinData] = useState(data)
    const [refresh, setRefresh] = useState(false)
    const [prizeNumber, setPrizeNumber] = useState(0)
    const [picked, setPicked] = useState([])
    const [isSpinning, setIsSpinning] = useState(false)
    const [hideText, setHideText] = useState(false)

    const changePerson = (p, i) => {
        let peopleNew = people
        let spinDataNew = spinData

        let rep1 = p.replace('.', '')
        let rep2 = rep1.replace(',', '')
        peopleNew[i].name = rep2
        if (!peopleNew[i].tagged && p.includes('.')) {
            peopleNew[i].tagged = true
            setAmtTagged(amtTagged + 1)
        }
        if (!peopleNew[i].isFirst && p.includes(',')) {
            peopleNew[i].isFirst = true
            setAmtFirst(amtFirst + 1)
        }
        spinDataNew[i].option = rep2

        setSpinData(spinDataNew)
        setPeople(peopleNew)
        setRefresh(!refresh)
    }

    const changeInputAmount = (e) => {
        if (isNaN(e.target.value)) return alert("Not a valid number")
        if (e.target.value > 50) return alert("Too many people!")
        loadInputs(e.target.value)
    }

    /**
     * Update the inputs list
     */
    const loadInputs = (amount) => {
        let peopleNew = people
        let spinDataNew = spinData
        let extraSize = amount - people.length
        if (extraSize > 0) {
            for (let i = 0; i < extraSize; i++) {
                peopleNew.push({name: "", tagged: false, picked: false, isFirst: false})
                spinDataNew.push({option: "", style: {backgroundColor: getRandomColor(), textColor: 'black'}})
            }
        } else {
            for (let i = 0; i < -1 * extraSize; i++) {
                if (peopleNew[peopleNew.length - 1].tagged) {
                    setAmtTagged(amtTagged - 1)
                }
                if (peopleNew[peopleNew.length - 1].isFirst) {
                    setAmtFirst(amtFirst - 1)
                }
                if (peopleNew[peopleNew.length - 1].picked) {
                    setAmtPicked(amtPicked - 1)
                }
                peopleNew.pop()
                spinDataNew.pop()
            }
        }
        for (let i = 0; i < peopleNew.length; i++) {
            spinDataNew[i].style.backgroundColor = getRandomColor()
            peopleNew[i].picked = false
        }

        setAmtPicked(0)
        setSpinData(spinDataNew)
        setPeople(peopleNew)
        setRefresh(!refresh)
    }

    const deleteOption = (i) => {
        let peopleNew = people
        let spinDataNew = spinData
        spinDataNew.splice(i, 1)
        peopleNew.splice(i, 1)
        setSpinData(spinDataNew)
        setPeople(peopleNew)
        setRefresh(!refresh)
        loadInputs(peopleNew.length)
    }

    const checkInputs = () => {
        for (let i = 0; i < people.length; i++) {
            if (spinData[i].option.length < 1) {
                alert("not all inputs are filled in correctly")
                return false
            }
        }
        return true
    }

    const spinWheel = () => {
        if (isSpinning) return
        pressHide()
        if (!checkInputs()) return
        let options = []
        let peopleNew = people

        if (amtPicked >= peopleNew.length) return alert("maximum has been reached")

        let hasIsFirst = false

        for (let i = 0; i < peopleNew.length; i++) {
            if (peopleNew[i].picked) continue
            let notTagged = !peopleNew[i].tagged
            let isFirst = peopleNew[i].isFirst
            let outOfOptions = amtPicked + amtTagged >= peopleNew.length
            if (isFirst) {
                hasIsFirst = true
                options.push(i)
            } else if ((notTagged || outOfOptions) && !hasIsFirst) {
                options.push(i)
            }
        }

        let choiceIndex = options[Math.floor(Math.random() * (options.length))]
        peopleNew[choiceIndex].picked = true
        setAmtPicked(amtPicked + 1)

        setPeople(peopleNew)
        setPrizeNumber(choiceIndex)
        setMustSpin(true)
        setRefresh(!refresh)
        setIsSpinning(true)
    }

    const afterSpinning = () => {

        let spinDataNew = spinData
        let pickedNew = picked
        pickedNew.push(people[prizeNumber].name)
        spinDataNew[prizeNumber].style.backgroundColor = 'white'

        setIsSpinning(false)
        setMustSpin(false)
        setSpinData(spinDataNew)
        setPicked(pickedNew)
    }

    const pressHide = () => {
        setHideText(true)
    }

    return (<>
            <Container fluid
                       style={{
                           paddingLeft: 50,
                           paddingRight: 50,
                           paddingTop: 20,
                           paddingBottom: 20,
                           backgroundColor: '#0d5174',
                           justifyContent: "center",
                           display: "flex"
                       }}>

                <Container>
                    <CardCenter>
                        {!hideText ? (<><h1>Rigged Wheel Decider!</h1>
                            <h2>Click below to hide this message and troll your friends!!</h2>
                            <h3 style={{color: "red"}}>Instructions: include a dot into the name, and those names will
                                be chosen last!</h3>
                            <Button onClick={pressHide}>Hide</Button></>) : (<>
                            <h1>Wheel Decider</h1>
                            <h2>Enter some names, and click spin</h2>
                        </>)}

                    </CardCenter>
                    <Row className={"justify-content-center"}>
                        <a onClick={spinWheel}>
                            <Wheel
                                mustStartSpinning={mustSpin}
                                prizeNumber={prizeNumber}
                                data={spinData}
                                onStopSpinning={afterSpinning}
                                backgroundColors={['#3e3e3e', '#df3428']}
                                textColors={['#ffffff']}
                                innerRadius={20}
                            />
                        </a>
                        <Ruimte y={1}/>
                        <Button onClick={spinWheel} disabled={isSpinning}
                                type="primary" size={'large'}>Spin</Button>
                        <Ruimte y={1}/>
                    </Row>


                    {picked.length > 0 &&
                    <CardCenter title={"List of people picked"} style={{margin: 5}}>
                        {picked.map((p, i) => {
                            return (<>
                                <h3>{Number(i + 1)}. {p}</h3>
                            </>)
                        })}
                    </CardCenter>
                    }

                    <CardCenter title={"Amount of people"} style={{margin: 5}}>
                        <Input placeholder="Amount of people" onChange={changeInputAmount}/>
                    </CardCenter>
                    <CardCenter title={"Options"} style={{width: 300, margin: 5}}>
                        {people.map((val, i) => {
                            return (<div style={{display: "inline-flex"}} key={i}>
                                    <Input
                                        placeholder={"Enter name"}
                                        key={i}
                                        value={people[i].name}
                                        onChange={(e) => changePerson(e.target.value, i)}
                                        style={{marginBottom: 5}}
                                    />
                                    <Ruimte x={1}/>
                                    <Button type="danger" onClick={() => deleteOption(i)}>X</Button>
                                </div>
                            )
                        })}
                    </CardCenter>
                    <Ruimte y={5}/>


                    <CardCenter title={"About"} noCenterText style={{width: 500}}>
                        <p>Made by StudioCreations</p>
                        <Ruimte y={2}/>
                        <Timeline>
                            <Timeline.Item>Original idea 2012-09-01</Timeline.Item>
                            <Timeline.Item>Technical testing 2016-09-01</Timeline.Item>
                            <Timeline.Item>Website Launch 2017-09-01</Timeline.Item>
                        </Timeline>
                    </CardCenter>
                </Container>
            </Container>
        </>
    )
}

export default App


const data = [
    {option: '', style: {backgroundColor: '#2bff2a', textColor: 'black'}},
    {option: '', style: {backgroundColor: '#fb87e1', textColor: 'black'}},
    {option: '', style: {backgroundColor: '#28d6fd', textColor: 'black'}}
]

const peopleData = [
    {
        name: data[0].option,
        tagged: false,
        picked: false,
        isFirst: false,
    }, {
        name: data[1].option,
        tagged: false,
        picked: false,
        isFirst: false,
    }, {
        name: data[2].option,
        tagged: false,
        picked: false,
        isFirst: false,
    }]

function getRandomColor() {
    var letters = '0123456789ABCDEF'
    var color = '#'
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)]
    }
    return color
}