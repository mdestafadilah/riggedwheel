import React from "react"
import PropTypes from "prop-types"
import Ruimte from "../Space"
import {Row} from "react-bootstrap"
import {Card} from "antd"

export default function CardCenter(props) {

    let classNameCenter = props.noCenter ? "" : "justify-content-center"
    let classNameText = props.noCenterText ? "" : " text-center"
    return (<>
            {props.top ? <Ruimte y={props.top}/> : <Ruimte y={1}/>}
            <Row
                className={classNameCenter + classNameText}>
                <Card title={props.title} className={props.className ? props.className : null} style={props.style ? props.style : null}>
                    <Ruimte y={.5}/>
                    {props.children}
                </Card>
            </Row>
            {props.bottom ? <Ruimte y={props.bottom}/> : <Ruimte y={1}/>}
        </>
    )
}

CardCenter.propTypes = {
    top: PropTypes.number,
    bottom: PropTypes.number,
    className: PropTypes.string,
    noCenter: PropTypes.bool,
    noCenterText: PropTypes.bool,
    title: PropTypes.string
}