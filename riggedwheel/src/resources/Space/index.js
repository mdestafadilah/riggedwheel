import React from "react"
import PropTypes from "prop-types"

export default function Space(props) {
    if (props.y && props.y > 0 && props.y < 100) {
        return (<div className={props.className} style={{width: "100%", height: 20 * props.y}}/>)
    } else if (props.x && props.x > 0 && props.x < 100) {
        return (<div className={props.className} style={{height: "100%", width: 20 * props.x}}/>)
    } else return (<div className={props.className} style={{width: "100%", height: 20}}/>)
}

Space.propTypes = {
    x: PropTypes.number,
    y: PropTypes.number
}